package com.example.produtos;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

/**
 * Created by csalmeida on 13/08/2017.
 */

public class ProdutosDetailsFragment extends Fragment{

    // Cria uma nova instancia de detailsfRagment
    public static ProdutosDetailsFragment newInstance(int index) {
        ProdutosDetailsFragment cdf = new ProdutosDetailsFragment();
        Bundle args = new Bundle();
        args.putInt("index", index);
        cdf.setArguments(args);
        return cdf;
    }
    public int getShowIndex() {
        return getArguments().getInt("index",0);
    }
    // Devolve um objecto ScrolView com um WebView carregado com a pagina Web
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle saveInstanceState) {
        final String[] produtos = getResources().getStringArray(R.array.produtos);

        String produto = produtos[getShowIndex()];

        ScrollView scroller = new ScrollView(getActivity());

        LinearLayout ll = new LinearLayout(getActivity());
        ll.setOrientation(LinearLayout.VERTICAL);

        // Create TextView
        TextView product = new TextView(getActivity());
        product.setText(" Produto " + produto + "    ");
        ll.addView(product);

        // Create TextView
        TextView price = new TextView(getActivity());
        int preco = getShowIndex() * 100;
        price.setText("  $ " + preco  + "     ");
        ll.addView(price);



        //WebView webview = new WebView(getActivity());
        //webview.setWebViewClient(new  SwAWebClient());
        //int padding =
        //        (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,4,getActivity().getResources().getDisplayMetrics());
        //webview.setPadding(padding,padding,padding,padding);
        scroller.addView(ll);
        //webview.loadUrl(links[getShowIndex()]);





        return scroller;
    }
}
